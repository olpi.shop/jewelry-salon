package com.olpi.domain;

import java.util.Objects;

import com.olpi.enums.ClarityScale;

public abstract class Stone implements Comparable<Stone> {

    private int priceInCents;
    private double caratWeight;
    private ClarityScale clarity;

    public Stone(int priceInCents, double caratWeight, ClarityScale clarity) {
        this.priceInCents = priceInCents;
        this.caratWeight = caratWeight;
        this.clarity = clarity;
    }

    public int getPriceInCents() {
        return priceInCents;
    }

    public double getCaratWeight() {
        return caratWeight;
    }

    public ClarityScale getClarity() {
        return clarity;
    }

    public void setPriceInCents(int priceInCents) {
        this.priceInCents = priceInCents;
    }

    public void setCaratWeight(double caratWeight) {
        this.caratWeight = caratWeight;
    }

    public void setClarity(ClarityScale clarity) {
        this.clarity = clarity;
    }

    public int compareTo(Stone other) {
        if (this.priceInCents < other.priceInCents) {
            return -1;
        } else if (this.priceInCents > other.priceInCents) {
            return 1;
        }
        return 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(priceInCents, caratWeight, clarity);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Stone other = (Stone) obj;
        return priceInCents == other.priceInCents
                && caratWeight == other.caratWeight && clarity == other.clarity;
    }

    @Override
    public String toString() {
        return " price = " + String.format("%.2f", (double) priceInCents / 100)
                + "$, weight = " + String.format("%.2f", caratWeight)
                + " carat, clarity " + clarity.name() + ".";
    }

}
