package com.olpi.domain;

import java.util.Objects;

import com.olpi.enums.ClarityScale;
import com.olpi.enums.SemiPreciousStoneNames;

public class SemiPreciousStone extends Stone {

    private SemiPreciousStoneNames name;

    public SemiPreciousStone(SemiPreciousStoneNames name, int priceInCents,
            double caratWeight, ClarityScale clarity) {

        super(priceInCents, caratWeight, clarity);
        this.name = name;

    }

    public SemiPreciousStoneNames getName() {
        return name;
    }

    public void setName(SemiPreciousStoneNames name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, super.getPriceInCents(),
                super.getCaratWeight(), super.getClarity());
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        SemiPreciousStone other = (SemiPreciousStone) obj;
        return name.equals(other.name);
    }

    @Override
    public String toString() {
        return "SemiPreciousStone: name = " + this.getName() + super.toString();
    }

}
