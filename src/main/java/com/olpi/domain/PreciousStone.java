package com.olpi.domain;

import java.util.Objects;

import com.olpi.enums.ClarityScale;
import com.olpi.enums.PreciousStoneNames;

public class PreciousStone extends Stone {

    private PreciousStoneNames name;

    public PreciousStone(PreciousStoneNames name, int priceInCents,
            double caratWeight, ClarityScale clarity) {

        super(priceInCents, caratWeight, clarity);
        this.name = name;

    }

    public PreciousStoneNames getName() {
        return name;
    }

    public void setName(PreciousStoneNames name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, super.getPriceInCents(),
                super.getCaratWeight(), super.getClarity());
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        PreciousStone other = (PreciousStone) obj;
        return name.equals(other.name);
    }

    @Override
    public String toString() {
        return "PreciousStone: name = " + this.getName() + super.toString();
    }

}
