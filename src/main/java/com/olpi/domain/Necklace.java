package com.olpi.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Necklace {

    private static int idCounter = 10001;

    private int id;
    private String name;
    private List<Stone> stones;

    public Necklace(String name) {
        id = idCounter++;
        this.name = name;
        stones = new ArrayList<>();
    }

    public Necklace(String name, List<Stone> stones) {
        id = idCounter++;
        this.name = name;
        this.stones = stones;
    }

    public double getTotalPrice() {
        return (double) stones.stream().mapToInt(Stone::getPriceInCents).sum()
                / 100;
    }

    public double getTotalWeigh() {
        return stones.stream().mapToDouble(Stone::getCaratWeight).sum();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Stone> getStones() {
        return stones;
    }

    public void addStones(Stone stone) {
        stones.add(stone);
    }

    @Override
    public String toString() {

        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("Necklace:\nArticle: \"" + id + "\" | Name: " + name
                + " | stones:");

        for (Stone stone : stones) {
            joiner.add(stone.toString());
        }

        return joiner.toString();
    }

}
