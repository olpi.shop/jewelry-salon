package com.olpi.service;

import java.util.List;
import java.util.stream.Collectors;

import com.olpi.domain.Necklace;
import com.olpi.domain.Stone;
import com.olpi.enums.ClarityScale;

public class NecklaceService {

    public void sortStonesByPrice(Necklace necklace) {
        necklace.getStones().sort(Stone::compareTo);
    }

    public List<Stone> findStonesByClarity(Necklace necklace,
            ClarityScale maxClarity, ClarityScale minClarity) {

        return necklace.getStones().stream()
                .filter(c -> c.getClarity().getValue() <= minClarity.getValue()
                        && c.getClarity().getValue() >= maxClarity.getValue())
                .collect(Collectors.toList());
    }

}
