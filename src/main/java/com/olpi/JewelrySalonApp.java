package com.olpi;

import java.util.ArrayList;
import java.util.List;

import com.olpi.domain.Necklace;
import com.olpi.domain.PreciousStone;
import com.olpi.domain.SemiPreciousStone;
import com.olpi.domain.Stone;
import com.olpi.enums.ClarityScale;
import com.olpi.enums.PreciousStoneNames;
import com.olpi.enums.SemiPreciousStoneNames;
import com.olpi.service.NecklaceService;

public class JewelrySalonApp {
    public static void main(String[] args) {

        List<Stone> stones = new ArrayList<>();

        stones.add(new PreciousStone(PreciousStoneNames.ALEXANDRITE, 11025, 2.3,
                ClarityScale.VVS1));
        stones.add(new PreciousStone(PreciousStoneNames.DIAMOND, 15485, 1.5,
                ClarityScale.SI2));
        stones.add(new PreciousStone(PreciousStoneNames.RUBY, 5263, 1,
                ClarityScale.FL));
        stones.add(new PreciousStone(PreciousStoneNames.ALEXANDRITE, 8896, 1.8,
                ClarityScale.SI1));

        stones.add(new SemiPreciousStone(SemiPreciousStoneNames.AMETHYST, 2536,
                2.3, ClarityScale.VVS1));
        stones.add(new SemiPreciousStone(SemiPreciousStoneNames.BERYL, 4157,
                1.5, ClarityScale.SI2));
        stones.add(new SemiPreciousStone(SemiPreciousStoneNames.APATITE, 6574,
                1, ClarityScale.FL));
        stones.add(new SemiPreciousStone(SemiPreciousStoneNames.INDICOLITE,
                2165, 1.8, ClarityScale.SI1));

        Necklace necklace = new Necklace("Victoria", stones);
        System.out.println(necklace.toString());

        NecklaceService necklaceService = new NecklaceService();
        necklaceService.sortStonesByPrice(necklace);
        System.out.println("\nSorted stones:\n\n" + necklace.toString());

        stones = necklaceService.findStonesByClarity(necklace,
                ClarityScale.VVS1, ClarityScale.VS2);

        System.out.println("\nFound stones by clarity:\n");
        for (Stone stone : stones) {
            System.out.println(stone.toString());
        }

    }
}
