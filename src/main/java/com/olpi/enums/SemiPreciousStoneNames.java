package com.olpi.enums;

public enum SemiPreciousStoneNames {
    AQUAMARINE("Aquamarine"),
    AMETHYST("Amethyst"),
    APATITE("Apatite"),
    BENITOITE("Benitoite"),
    GARNET("Garnet"),
    DEMANTOID("Demantoid"),
    EREMEEVA("Eremeeva"),
    INDICOLITE("Indicolite"),
    KYANITE("Kyanite"),
    KUNZITE("Kunzite"),
    OPAL("Opal"),
    RHODOLITE("Rhodolite"),
    RHODOCHROSITE("Rhodochrosite"),
    SPODUMENE("Spodumene"),
    TAAFFEITE("Taaffeite"),
    TOPAZ("Topaz"),
    TOURMALINE("Tourmaline"),
    BERYL("Beryl"),
    ZIRCON("Zircon"),
    SPINEL("Spinel");

    private String name;

    SemiPreciousStoneNames(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
