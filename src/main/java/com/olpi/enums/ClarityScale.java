package com.olpi.enums;

public enum ClarityScale {
    FL(1),
    IF(2),
    VVS1(3),
    VVS2(4),
    VS1(5),
    VS2(6),
    SI1(7),
    SI2(8),
    I1(9),
    I2(10),
    I3(11);

    private int value;

    ClarityScale(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
