package com.olpi.enums;

public enum PreciousStoneNames {
    DIAMOND("Diamond"),
    ALEXANDRITE("Alexandrite"),
    EMERALD("Emerald"),
    RUBY("Ruby"),
    SAPPHIRE("Sapphire"),
    CHRISOBERILL("Chrisoberill");

    private String name;

    PreciousStoneNames(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
